import { TareaListComponent } from './../tarea-list/tarea-list.component';
import { TareaFormComponent } from './../tarea-form/tarea-form.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css']
})
export class TareaComponent implements OnInit {


  @ViewChild(TareaListComponent)
  lista: TareaListComponent;

  @ViewChild(TareaFormComponent)
  form: TareaFormComponent;

  @ViewChild("formTab") formTab: ElementRef;
  constructor() { }

  ngOnInit() {
    /*Al hacer click en la tabla, cargar formulario y cambiar tab*/
    this.lista.getSelectedDataSubject().subscribe(fila => {
      this.form.setEntity(fila);
      this.formTab.nativeElement.click();
    });
    /*Al borrar o agregar registro en formulario, actualizar lista */
    this.form.getEntityChangedSubject().subscribe(entity => {
       this.lista.refresh();
    })
  }

}
