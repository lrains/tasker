import { SolicitanteListComponent } from './../solicitante-list/solicitante-list.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SolicitanteFormComponent } from '../solicitante-form/solicitante-form.component';

@Component({
  selector: 'app-solicitante',
  templateUrl: './solicitante.component.html',
  styleUrls: ['./solicitante.component.css']
})
export class SolicitanteComponent implements OnInit {

  @ViewChild(SolicitanteListComponent) lista: SolicitanteListComponent;
  @ViewChild(SolicitanteFormComponent) form: SolicitanteFormComponent;
  @ViewChild("formTab") formTab: ElementRef;

  constructor() { }

  ngOnInit() {
    /*Al hacer click en la tabla, cargar formulario y cambiar tab*/
    this.lista.getSelectedDataSubject().subscribe(fila => {
      console.log("SolicitanteComponent -> Registro seleccionado:", fila);
      this.form.setEntity(fila);
      this.formTab.nativeElement.click();
    });

    /*Al borrar o agregar registro en formulario, actualizar lista */
    this.form.getEntityChangedSubject().subscribe(entity => {
      this.lista.refresh();
    })
  }

}
