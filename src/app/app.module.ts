import { SesionService } from './servicios';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SolicitanteComponent } from './solicitantes/solicitante/solicitante.component';
import { SolicitanteListComponent } from './solicitantes/solicitante-list/solicitante-list.component';
import { SolicitanteFormComponent } from './solicitantes/solicitante-form/solicitante-form.component';
import { AbmButtonsComponent } from './shared/abm-buttons/abm-buttons.component';
import { RouterModule } from '@angular/router';
import { CargoComponent } from './cargos/cargo/cargo.component';
import { CargoListComponent } from './cargos/cargo-list/cargo-list.component';
import { CargoFormComponent } from './cargos/cargo-form/cargo-form.component';
import { DepartamentoComponent } from './departamentos/departamento/departamento.component';
import { DepartamentoListComponent } from './departamentos/departamento-list/departamento-list.component';
import { DepartamentoFormComponent } from './departamentos/departamento-form/departamento-form.component';
import { misRutas } from './app.routing';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PersonalComponent } from './personales/personal/personal.component';
import { PersonalFormComponent } from './personales/personal-form/personal-form.component';
import { PersonalListComponent } from './personales/personal-list/personal-list.component';
import { TareaComponent } from './tareas/tarea/tarea.component';
import { TareaListComponent } from './tareas/tarea-list/tarea-list.component';
import { TareaFormComponent } from './tareas/tarea-form/tarea-form.component';
import { ClienteComponent } from './clientes/cliente/cliente.component';
import { ClienteListComponent } from './clientes/cliente-list/cliente-list.component';
import { ClienteFormComponent } from './clientes/cliente-form/cliente-form.component';
import { AuthGuard, CustomInterceptor } from './interceptors';

@NgModule({
  declarations: [
    LoginComponent,
    AdminLayoutComponent,
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    SolicitanteComponent,
    SolicitanteListComponent,
    SolicitanteFormComponent,
    AbmButtonsComponent,
    CargoComponent,
    CargoListComponent,
    CargoFormComponent,
    DepartamentoComponent,
    DepartamentoListComponent,
    DepartamentoFormComponent,
    DashboardComponent,
    PersonalComponent,
    PersonalFormComponent,
    PersonalListComponent,
    TareaComponent,
    TareaListComponent,
    TareaFormComponent,
    ClienteComponent,
    ClienteListComponent,
    ClienteFormComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(misRutas, { useHash: true }),
    HttpClientModule,
    FormsModule
  ],
  providers: [AuthGuard, SesionService, {
    provide: HTTP_INTERCEPTORS,
    useClass: CustomInterceptor,
    multi: true
  },],
  bootstrap: [AppComponent]
})
export class AppModule { }
