import { Subject } from 'rxjs';
import { ClienteService } from './../../servicios';
import { Cliente } from './../../modelos';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.css'],
  providers: [ClienteService]
})
export class ClienteListComponent implements OnInit {
  clientes: Cliente[] = [];

  selectedDataSubject = new Subject();

  constructor(private clienteService: ClienteService) {
  }
  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.clienteService.findAll().subscribe(resp => {
      this.clientes = resp
    })
  }

  selectData(data) {
    console.log('ClienteListComponent.selectData: ', data);
    this.selectedDataSubject.next(data);
  }

  getSelectedDataSubject() {
    return this.selectedDataSubject.asObservable();
  }

}
