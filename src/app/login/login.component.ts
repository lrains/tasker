import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SesionService } from '../servicios';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private sesionService: SesionService) { }



  returnUrl = '/'
  usuario = { username: 'admin', password: 'adm' }
  errorMsg = ''
  showLoginBtn = true
  ngOnInit() {

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] + 'coreee' || '/';

    this.checkSession()
  }

  login() {
    this.showLoginBtn = false
    this.sesionService.login(this.usuario).subscribe(resp => {
      console.log("login", resp);

      this.sesionService.setSesionInfo(resp)
      if (this.returnUrl) {
        this.router.navigate([this.returnUrl]);
      } else {
        this.router.navigate(['dashboard']);
      }
    }, resp => {
      if (resp.status == 401) {
        this.errorMsg = "Usuario o contraseña incorrectos";
      } else if (resp.status == 409) {
        this.errorMsg = resp.error.msg;
      } else {
        this.errorMsg = "Ocurrió un error en el servidor";
      }
      this. showLoginBtn = true
    });
  }

  checkSession() {

    this. showLoginBtn = false
    this.sesionService.checkSession().subscribe(resp => {
      console.log("Checking sessionInfo in login:", resp);
      this. showLoginBtn = true
      if (resp.usuario) {
        this.sesionService.setSesionInfo(resp);
        if (this.returnUrl) {
          this.router.navigate([this.returnUrl]);
        } else {
          this.router.navigate(['dashboard']);
        }
      }
    }, resp => {

      this. showLoginBtn = true
    });
  }

}
