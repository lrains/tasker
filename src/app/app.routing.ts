import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { ClienteComponent } from './clientes/cliente/cliente.component';
import { PersonalComponent } from './personales/personal/personal.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes } from '@angular/router';
import { SolicitanteComponent } from './solicitantes/solicitante/solicitante.component';
import { DepartamentoComponent } from './departamentos/departamento/departamento.component';
import { TareaComponent } from './tareas/tarea/tarea.component';

export const misRutas: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: '', component: AdminLayoutComponent, children: [
            { path: 'solicitantes', component: SolicitanteComponent },
            { path: 'departamentos', component: DepartamentoComponent },
            { path: 'dashboard', component: DashboardComponent },
            { path: '', component: DashboardComponent },
            { path: 'personales', component: PersonalComponent },
            { path: 'tareas', component: TareaComponent },
            { path: 'clientes', component: ClienteComponent }
        ]
    },
    {path: '**', redirectTo: '/'}, //si se ingresa ruta errónea, dirigir a home
]

